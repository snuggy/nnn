## Truth

Billions of people use pornography, with hundreds of millions currently trying to quit. Porn is really bad for you; *any* amount desensitises the brain because we can't handle endless novelty.

This desensiting gets worse over time: you'll need more porn or different harder genres for the same hit. At the same time, our primal reward system begins trimming other receptors, and depression, anxiety, stress and many other conditions are often *primary* symptoms of porn use.

Fortunately, society has begun questioning porn.

It's early days -- almost counterculture -- but smoking was once normalised too. The science is absolute, with [many](https://reddit.com/r/nofap) [online](https://reddit.com/r/pornfree) [communities](https://reddit.com/r/semenretention) [discussing](https://reddit.com/r/pureretention) [porn's](https://yourbrainrebooted.com) [negative](https://fightthenewdrug.org) [effects](https://yourbrainonporn.com). Take a cursory look, and you'll find endless oceans of people discussing quitting attempts.

Few are succeesful.

Attempts using willpower -- anything involving counting, blocking, dieting, rewarding or punishing -- generally fail because we don't use pornography for the reasons we should stop. Reframing the reasons why we continue using porn ends the 'tug of war' and quitting becomes easy and enjoyable.

Simply put, 'porn addicts' don't exist, there's just varying degrees of addiction. Those realising they've been hooked are stigmatised because using porn is heavily normalised; but we stigmatise few addictions -- we don't call smokers 'nicotineaholics', or tell methamphetamine users to 'just cut down' -- and consider them as public health issues.

We're seeing the effects today. I speak to school nurses who have teenage girls crying in their offices because their *eleven* year old boyfriends are pressuring them for anal sex.

It's not just the younger generation though; we might think of the [partners](https://reddit.com/loveafterporn) of middle-aged porn users, their familes, children, communities, jobs and lives. Entire countries -- including religious ones -- desensiting themselves normal life.

Still, the tide is turning.

Celebrties, 'No Nut November', media coverage, memes and communities have started conversations.

Zero willpower approaches, like those pioneered by Allen Carr of smoking cessation fame have helped tens of millions of people worldwide.

Frankly, I'm lucky to have found it. I've never been a smoker, but I used to use porn, and I stumbed upon a bloke who had hacked together Allen Carr's *EasyWay* for porn. It was hosted on Google Sites and freed me from the porn addiction's "brainwashing" and life's been wonderful ever since.

Still, it was really poorly written, so on a family holiday to New Zealand I used two e-ink tablets for heavily editing and rewriting it. I've released it completely free and open source online, getting around 60,000 visitors a month.

People have shared, translated and created resources about it, and it's regarded as the only way of truly quitting.

Still, Allen Carr isn't a writer. [easypeasymethod.org](https://easypeasymethod.org) works, but you'll find reading to be a slight chore. My honest criticism is that it rabbits on too long, bereates you, and relies heavily on affirmation than true understanding.

By 'No Nut November' it's becoming [basedmethod.org](https://gitlab.com/snuggy/based) -- a simply written rewrite combining psychological theories, resources and understandings. I'm using people's memories, imaginations and guiding them to their own realisations to make it more of an experience. I'm still working on it, but my progress is on the [gitlab repository](https://gitlab.com/snuggy/based).

It will be releasd by early October, probably earlier. It's honestly very fun writing it -- it uses pictures much more liberally, and t's simpler, shorter and punchier.

As such, it's far easier to read, share and translate.

## Urbit

In an entirely different vein, [Urbit](https://urbit.org/overview) is a drop in replacement for the internet and computing itself.

I fell in love with it after this quote.

> "*We think the internet can’t be saved. The way things are going, MEGACORP will always control our apps and services because we can no longer run them ourselves.*
>
> *The only way out of this mess is with a completely new platform that’s owned and controlled by its users.*"

Urbit is a new type of computer, network and identity system that you can run anything on. It solves every problem with modern computing. You can imagine it as running your own little tiny computer that never breaks, and it's so simple that my mum could run it.

It's a friendlier network because there's only four billion identities, making spamming have a higher cost than it's return. I've explained it [to my mum](https://youtu.be/wzbakmWJMaA) here, and she vibes with it.

Most excitingly, developing is *much* easier because there's less moving parts. Combining Urbit's simplicity with frameworks like [Svel](https://svelte.dev) [(explained)](https://youtu.be/rv3Yq-B8qp4) means extremely quick turnarounds.

I see Urbit's uptake as inevitable because it fixes all problems with the modern internet. Regardless of the current proposal, Urbit is the next big thing.

## Against porn, today.

Growing numbers of people are quitting porn. You might know of tongue-and-cheek 'No Nut November' or have learnt about the health effects, read news articles, seen celebrities speaking out, or chuckled at a meme.

It's still early days, but it's happening. Here's some of the players in the space.

[Your Brain on Porn](https://yourbrainonporn.com) -- A great resource for neuroscience on porn's effects.

[Fight the New Drug](https://fightthenewdrug.org) -- Focusing on youth education by showing the effects of porn on brains, hearts and the world around us.

Various forums -- Hosting communities of people sharing their quitting porn journeys.

- Your Brain Rebooted
- Reboot Forum

[NoFap](https://nofap.com) -- A trademarked company primarily promoting streaking counting approaches. They're responsible much of getting quitting porn mainstream. The name refers to the practice of obstaining from masturbation, and is trademarked to prevent unauthorised porn industry use.

[Sex Addicts Anonymous](https://sa.org) -- Uses a '12 step' approach focusing on recovery, rather than quitting directly.

[The 'Coomer'](https://coomer.org) -- A meme gaining popularity in 2017 showing a 'wojak' (self-representation) warped due to pornography. It gained most traction on [reddit](https://reddit.com/r/coomer), but the very loosely moderated subreddit succumbed to hatred, and it was banned from the platform. I now host a filtered repository of memes at <https://coomer.org>

#### Celebrities mentioning it

**Terry Crews** -- *"You know, for years, years, years, my dirty little secret was that I was addicted to pornography, for years. It’s kind of crazy because this thing has become a problem. I think it’s a worldwide problem, but pornography, it really, really, messed up my life in a lot of ways. Some people deny it. They say, “Hey man, you can’t really be addicted to pornography. There’s no way.” But I’m going to tell you something, if day turns into night, and you are still watching, you probably got a problem. And that was me. It affected everything. I didn’t tell my wife. Didn’t tell my friends, nobody knew. But the internet allowed that secret to stay and grow.*

*My wife was literally like, “I don’t know you anymore. I’m outta here.” That changed me. I had to change because I realized, “Yo, this thing is a major, major problem.” I literally had to go to rehab for it.” And the thing what I found was that by not telling people, it becomes more powerful, but when you tell, and when you put it out there in the open, just like I’m doing right now, to the whole world, it loses its power. And everybody wants you to keep this little secret. I’m telling, and I’m putting it out there. I’ve been free of this thing going on six, seven years now. Thank goodness. But now, it’s become my battle to help other people who are going through the same thing.*"

**Russell Brand** -- *"We’re living in this culture now where there’s just icebergs off filth floating through every house on Wi-Fi. It’s inconceivable what it must be like to be a young adolescent now with this kind of access to porn. I know that pornography is wrong, that I shouldn’t be looking at it. There’s a general feeling, isn’t there, in your core, if you look at pornography, that this isn’t what’s the best thing for me to do."*

**Rashia Jones** -- *"I wrote this article for Glamour magazine where I was highly critical of how, like, how naked everybody is publicly right now, from pop stars, to kind of reality stars, whatever. I’m more concerned the very young fans of these girls who just emulate them, and, you know, the reason that these girls feel like this is a viable option, as the thing to do when you turn 18 and you leave high school. It’s because, culturally, porn is mainstream now."*

**Kanye West** -- *"Playboy was my gateway into full-on pornography addiction. My dad had a Playboy left out at age five, and it’s affected almost every choice I made for the rest of my life, from age five to now having to kick the habit. And it just presents itself in the open, like it’s okay. And I stand up and say, “You know, it’s not okay."*

**Chris Rock** -- *"One of the things about that I’ve done in the last whatever year, not just going to therapy, I’ve kind of gotten off social media. I got off all social media. I don’t watch pornography anymore. I’m like, my brain is like “Ahh.” Like, I’m focused, man. Now, so, a lot of stuff on YouTube or, what is it, Instagram, I got like a company that puts that stuff out."*

**Billie Elish** -- *"I’m so angry that porn is so loved, and I’m so angry at myself for thinking that it was okay. As a woman, I think porn is a disgrace. I used to be like the person that would like talk about porn all the time. I’d be like, “Oh, it’s so stupid that anybody would think that porn is bad or [cuts out] up. I think it’s so cool. It’s great.” I think it really destroyed my brain, and I feel incredibly devastated that I was exposed to so much porn."*

**Naval Ravikant** -- *"Social media, they’ve massaged all the mechanisms to addict like a Skinner’s pigeon [WWII experiment] or a rat, who’s just going to click, click, click, click. Can’t put the phone down. Food. They’ve taken sugar, and they weaponized it. They put it into all these different forms and varieties that you can’t resist eating. Drugs. They’ve taken pharmaceuticals and plants, and they’ve synthesized them. They’ve grown them in such a way that you get addicted. You can’t put them down. Porn. If you’re a young male and you wander on the internet, it’ll like sap away your libido, and your not going out in real life society anymore, because you got this incredibly stimulating stuff coming at."*

**Pamela Anderson** -- *"I’m concerned about porn addiction. I think it’s affecting relationships. You know, when you have a woman lying in bed in lingerie, and you’re in the bathroom looking at a computer, something’s wrong, you know? There’s so much access, and I’m worried about that for young people. People need more and more to get aroused, and I think it’s leading to violence against women, rape, child abuse. I really think it has something to do with that. And I think we really have to start thinking about what we’re doing as people, how we’re imprinting ourselves. What we’re watching, what we’re doing, what we’re eating, what we’re wearing. It all builds our character. And it’s really important to just have the conversation."*

**Kirk Franklin** -- *"There’s an anger that rises up in me. And I got evangelically ticked off about the fact that I wish somebody would have taught me a long time ago the repercussions of sex and flesh and lust and vanity and pride and ego. I wish somebody would have been holding my little behind accountable years ago. If I have been set free from this one, anybody can, because for years, I even questioned, “Could I get free from this?”"*

**Jordan Peterson** -- *"The typical porn actress, not the amateurs, but the professionals, have their sexually provocative physical elements exaggerated. And so, men are very visual, in terms of their sexual processing, and so, you know, the guys are pulled into it, and they’re pulled into it also by curiosity, but I think that ethically, it’s not good. It’s not good."*

**Jada Pinkett Smith** -- *"Back in the day, porn for my generation started in magazines, and then you had to like go to a CD store to get a VHS store. We didn’t have access to it like that. You had to like really go on an adventure to go see some pornography, whereas for your generation, it’s on your phone. It’s porn in your pocket.*"

**Mike Tyson** -- *"I don’t have a phone, ‘cause my phone is my lower self. I might look at porn, so I’m conscious of my lower self, so I know I don’t need a phone. I want to conquer the world, and I know I can’t conquer the world unless I conquer myself. I don’t live my life right all the time, but when I have something, when I desire something, I have to clean my life up. God won’t bless me if I’m dirty. If I’m cheating on my wife. If I’m doing something disrespectful to my family. He won’t bless me."*

**Andrew Huberman** -- *"If people are pursuing pornography, and they’re not pursuing relationships, there is the potential that they reach their twenties and thirties, and they are truly dysfunctional."*

**Shelley Lubben** -- *"Porn has become so violent, it’s just about hate. It’s just hate and violence."*

**Joe Rogan** -- *"Here’s a big one. Why are these girls doing this? Okay, here’s something that people don’t like to admit that enjoy porn. The vast majority of them have been molested. The vast majority. Some study they did on girls who get into porn, who’ve been sexually abused, mentally abused, and physically abused–it was overwhelming. It was overwhelming."*

**Gary Wilson** -- *"The truth is real life isn’t like those simplistic fantasies that you see in a pornography clip... You really have to replace porn with other activity. You have to just get away from the computer and do other things. I personally don’t want to have to be afraid to be aroused. I’d rather connect with my wife and get my arousal that way."*


### Campaigns

Most campaigns only achieve modest success because quitting still feels difficult. Teenagers understand porn harms them, but "plenty of things do" -- as nowadays it's normal being addicted to substances or screens.

Please don't get me wrong, informational sites are very useful and are why I started questioning porn in the first place; they just need to be paired with effective approaches, otherwise people are left in depressing relapse cycles.

Here's three campaigns of varying structure -- Allen Carr, No Nut November and the 'Coomer' meme -- for understanding

### Allen Carr

Allen Carr chainsmoked a hundred cigarettes a day for over 20 years until discovering EasyWay. Since then his bestselling books, courses and clinics have helped tens of millions. They have many celebrity endorsements, testimonials and have success rates of over 90% with money-back guarrantees. At long last, policy-makers are beginning to take notice, and since 2020 they've been working with the World Health Organization.

It's clear EasyWay is effective, but having been transforming lives for over XX years, why has it taken so long to break into the public sphere?

A guilty pleasure of mine is browsing the [4channel.org fitness board](https://4channel.org) because it provides a snapshot of a relatively productive online counterculture. Many people discuss nicotine cessation, or even the proported benefits of smoking, and Allen Carr is seldom recommended.

However, i's highly like someone posting about being trapped in porn addiction will be recommended [easypeasymethod.org](https://easypeasymethod.org) because of it's lower barriers for reading and sharing.

Quitting smoking using EasyWay means either skeptically purchasing a video course, buying a book, or pirating either. Many sites have made book piracy trival, but EasyWay's monetarily and attention barriers are still too high for most attention spans (and rationalised budgets).

### No Nut November

No Nut November is a 'memeable' trend where mostly young men discuss abstaining from masturbation, ejaculation and sexual intercourse during the month of November.

It's popular on [reddit](https://reddit.com/r/nonutnovember), with over XXXXXXX subscribers. The subreddit opens during November, and users discuss tips for abstaining and share memes.

Having gone on so long, it often gathers media attention.

Many influencers are also proponents, further spreading the awarenss.

This apporach works because it frames abstinence -- albeit temporary -- as fun and social, but day counting still makes people feel they're being deprived of something. Hence, a popular meme towards the end of the month is 'Destroy Dick December'.

### Coomer
	meme
	wojack
	virality
	hatred





## Internet

We didn't imagine computers would be normal. Encompassing rooms, they were for military and university use. Soon, they began spreading to workplaces, families and people.

Early communication involved connecting to bulletin boards or internet relay chat servers run by actual humans. Getting your message there required servers in between, which wasn't a problem because the internet was small.

Now we're at smart toilets, still using the same foundation as the 1970s.

The internet is complex -- my mum can't run a modern datacentre -- so companies have stepped in to make it easy to use.

Here's analysis of some of the players in the space.

### Facebook

Facebook uses your data for selling you ads.

I believe Facebook has it's marketshare for main three reasons.

1. **Ease** -- My mum can use it and it has all the things she needs. Any advantages Facebook has are just those of the internet itself but Facebook has put them in one central place.

2. **Ideas** -- Facebook multiplied quickly through
	- *0.facebook.com* allowing people to use Facebook for free when the internet was first becoming normalised.
	- *Free phones and internet projects* for accessing only Facebook in developing countries, polarising and sending many nations into civil unrest.
	- *Network effect* meant that when heaps of people started using it, more did too.

3. **Addictiveness** -- Teams of people are weaponising every aspect of Facebook -- from messaging to marketplace -- to keep people on the platform. We're aware of how Facebook uses casino techniques to keep people scrolling, but it's important to consider that *every feature*, like chat apps, reactions and stories, is for keeping people on Facebook.

It's losing this marketshare slowly (with people going to Instagram) because it isn't cool anymore.

MORE FACEBOOK DATA HEREXXX

### Discord

Gained popularity with gamers as an alternative Skype or Teamspeak. Nowadays they're attempting spreading through universities and small communities.

Discord makes it's money through keeping people invested in online relationships. 'Discord Nitro' subscriptions unlock more features and profile flairs and allow greater identification with online communities by 'boosting' them.

It's tagline *"Imagine a place... where you can belong to a school club, a gaming group, or a worldwide art community. Where just you and a handful of friends can spend time together. A place that makes it easy to talk every day and hang out more often.*" feels somewhat dystopic.

Discord doesn't include gamified conversational features because it doesn't have to. Users often customise their chatrooms by adding bots incentivising online participation with ranks and tiers of access.

[user base and statistics]

### TikTok

The Chinese Communist Party -- an evil genocidal dictatorship -- has created a highly addictive novelty driven dopamine desensitising platform.

I once tried Tiktok for half an hour to investigate hosting art films, and found it quite literally numbed my brain and I had to have a cold shower afterwards.

If Facebook lacerates your attention then TikTok mutilates it.

## Urbit

Fortunately, Urbit can recreate more respectful alternatives without massive corporations invovement. This would be a stopgap; Urbit is designed to respect you, so it's important shifting culture away from supernormal stimuli.

People using Urbit are early adopters

[URBIT CURRENTLY XXX]
	what's currently on urbit
		bunch of communities

Urbit envisions your urbit as the only computer you'd ever need, and your urbit planet as a 'civilisational key'. If your Urbit ID was a

## Social issues

### Addiction

It's impossible fully considering addiction's impact.

No statistics, but in order: it's alcohol, methamphetine and heroin.

Here's a hitchhiking story instead.

A bloke picked me up who used to work in Juneau's homelessness shelter. He said he never had a problem with people doing heroin, or meth (who were helpful cleaning), but that alcohol caused nearly every problem.

This is echoed by police officers I've spoken with, and it's the biggest contributing factor in accidents, assaults, domestic violence cases, murders and suicides.

Alcohol is *far* more harmful than heroin -- a medicine -- and far more addictive. Most 'casual' heroin users smoke it with a 5% chance of become dependent, and some inject it -- with a 33% chance of dependence. You can't die from opiate withdrawal either, it's a relatively safe drug.

It sounds unbelievable, but yes, most of what we know about stigmatised drugs is wrong. Here's an assignment I did on it. XXX LINK ASSIGNMENT. For a well rounded look at different psychoactives, *Drug Use for Grown Ups* by Dr Carl Hart is a good read.

I also like Jason Vale's *Kick the Drink... Easily!*, presenting extremely compelling arguements against alcohol. I never had a problem with drinking, but his convinced me to stop forever. I've been a happy non-drinker since the beginning of 2021, and it's been smooth sailing ever since. The book isn't free, but could be made open source.

### Homelessness

Homelessness is too complex for me understand at the moment, but I visited a Lutherian Food Bank in Juneau and spoke to Pastor XXXXX, who confirmed that most of the problems they have are because of alcohol.

*I'm in Alaska, where there's problems with getting enough food for homelessness shelters. This could be a distance issue, so I'll investigate further.*

### Consumption

novelty

food, products
Online media
thought speed


## Needs

From these observations, I feel a calmer and more compassionate society, free from addictions and less consumption prone would be better.
