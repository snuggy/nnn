[easypeasy method](https://easypeasymethod.org) frees people from pornography. Currently averaging [30,000 visitors](https://plausible.io/easypeasymethod.org) per month.

Soon, it'll become [basedmethod.org](https://basedmethod.org), rewritten simply and personally to be conversational. After finishing the book, people'll be feeling calmer and in control of their lives, and keen to make worldwide positive change.

One way is simply spreading awareness of porn's addictiveness. To bring you to reality, I speak to school nurses who have girls crying because their *eleven* year old boyfriends are pressuring theim for anal sex. Porn is addictive. It leads to escalation, desenitisation and second-hand effects. 'No Nut November' gives a global opportunity to change the future.

Indeed, I'm currently coordinating [semi-automated](https://weblate.org) [community translations](https://deepl.com), and people will be able to spread the word through posters, community outreach and conversations.

But what else might help the future?

We know our consumption, pollution and waste unsustainable now, and generations looking back will experience perplexed anger.

Change the narrative, this is what I propose.

Boycott golf.

I reckon we should end golf, turning the land
