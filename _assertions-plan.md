%
%%
# Assertions
%%
%

The assertions section is your chance to describe how you’re going to change things.

We will do X,
and then Y will happen.
We will build Z with this much money in this much time.
We will present Q to the market,
and the market will respond by taking this action.

	You’re creating tension by telling stories.
	You’re serving a specific market.
	You’re expecting something to happen because of your arrival. What?

This is the heart of the modern business plan.

The only reason to launch a project is to make change,
to make things better,
and we want to know what you’re going to do and what impact it’s going to have.

---
## vision

summary:

	quitting addictions
		hope
		tools
		cultural change

question consumption

urbit coordination
	tools
		listen
			chat with others, calmly
		find
			people doing things
		learn
			skills from others
		create
			things under tags??
		share (sell)
			other people's stuff

	barrier to entry
		spaceship
		bluetooth / wifi


	compounding
		improving local communities
		growing food
			potatoes
			donating
		homelessness
		"what's your urbit"

	no nut november
		awareness
			memes
		celebrities
			terry cews
			billie elish
		influencers
			trends
		media
			conversation already havig it
		local action
			posters
			outreach campaigns
	based
		freedom
		possible
		common scams
			myopia
		desire to help others
		themes
			anti-consumption
			practicality
				growing food at home
				learning skills
		alcohol
			open source

	based toolkit
		posters
		social media content
		letters
			politicans
			own schools
			universities
		influencers





### NNN
	will happen as usual
	celebrities
	springboard:
		publicity:
			celebrities
			news
			tech youtuber

	meme magic:
		coomer
			(banned)
			go to urbit

	who:
			young people
			organisations
				fight the new drug
				your brain on porn
				young people organisations

			islamic countries
			religious leaders
			clandestine operations


### based messaging
	// why not consumption?
		wall-e

		problems with tv
		supermarkets
		examples of ccp
			programming
			time to end ccp
			love
			peace is an option
			drop the wall
				what do they see?
			not based on truth
				cannot stand
			left right

### based change (toolkit)
	sharing based method
	urbit
	wacko schemes
		teams
	thinking
        posters
        local groups
        churches
        conversation
        schools
	sports groups



	For instance, they might print posters with a 'NNN Cheat Code', or share

	// what are the shared goals?
		ending porn

	// how will people calm their communitites.
		by sharing qutting pmo messaging
		organising local events
		attending local
		addiction resources
		feeling good

	// how to leave people loving each other
		waking up
		smallness
		critical thinking


### urbit
	listen
		chat with others, calmly
	find
		people doing things
	learn
		skills from others
	create
		things under tags??
	share (sell)
		other people's stuff


	urbit culture
		accepting
		cultural osmosis
		above
		because the alternative
			community fracture



### Addictions
	kick the drink easily
	easyway
		open source
		plain packaging


### Cultural shifts
	practical
	compassionate
	friendly



#### life
	switching friends to urbit
		chat program
		bluetooth / wifi
	critical thinking
	sam harris
	gardening at home
		donating
			homelessness shelters
				alcohol addiction
				volunteer
		marketing benefits
		helping out
		neighbours
		complexes

	hobbies:
		growing food
		yoga
		[other hobbies]

	chatting with others

### Cultural shifts
	dharma
	sharing
	kindness
	calming
