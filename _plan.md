# Proposal


## Summary

no nut november
	highlights the very real problem of porn addiction
	common misconception
		addictive substances
		varying lengths


based method
	simply
	calm people
	change the world

kick the drink... easily
allen carr's easyway

toolkit
	local action
	outreach ouline

urbit
	drop in replacement for all of computing and the internet itself
	tools to calm society

campaign

practical


---




%
%%
# Truth
%%
%

The truth section describes the world as it is.
	the market you are entering
	The more specific, the better.
	The more ground knowledge, the better.
	The more visceral the stories, the better.

	The point of this section is to be sure that you’re clear about how you see the world, and that you and I agree on your assumptions. This section isn’t partisan—it takes no positions; it just states how things are.

	Truth can take as long as you need to tell it. It can include spreadsheets, market share analysis, and anything else I need to know about how the world works.



### Addictions
	#### porn:
		vast majority using
		hundreds of millions trying to quit
		billions using
		neuroscience
		STORY: school nursing


		story:
			Allen Carr wrote easyway
			some anonymous bloke
			I rewrote (and rewriting again)
				free and open source
				30k visitors
				translated

			what it promises
				no willpower

			now rewriting to be mroe of an experience

		based method
			based method
				Allen Carr isn't a writer
					too long
					berrating
			design
				picture book
				simple
				conversation
				visualise
					excerpt from coomer
				realise themselves
				translating
					deepl
					weblate

			how
				goals for based
					simplicity
					sharability
					global
					practical


	no nut november
		trends
			good
			reddit
		memes
		media attention

		awareness
			memes
		celebrities
			terry cews
			billie elish
		influencers
			trends
		media
			conversation already havig it
                local action
                	posters
	outreach campaigns

		competitors in your space
		pmo:
			nofap
			online communities
			12 step
				where they fail
               		sexologists?

		porn industry
			impossible to conceive
			billions of dollars
			sex workers
			cams
			'hot girls wanted'

		and how others have succeeded and failed in the past.
			fight the new drug
			allen carr
				(money driven organisation)
				smear campaign
			yourbrainonporn
			coomer
			NNN


### Computers

We didn't imagine computers would get this big.

military
university
onboarding
home use


limtations


		how computing works
			brief history
				darpanet
				internet
					universities
					aol, eternal september
				complexity computers

		generational gerry rig


		[all other online websites]

#### Facebook

Universities
	much like the internet
0.facebook.com
easy
apps
smooth
addictive
messaging



#### discord

gaming
easy to use
	servers
	bots
tagline
gamified
nitro

#### tiktok
highly addictive
novelty
brain numbing story
ccp
genocidal dictatorship
permanent brain damage



## urbit

state of urbit
	car camping

	what's currently on urbit

	who currently uses urbit

		number
			tlon survey
		painting a picture of urbit user
			early adopter

recapping general purpose computer
	importance
	civilisational key


## social issues

### addiction
	impacts of addiction
	drug addiction stats
	reporting biasies
		alcohol is worse than heroin (unironically)
			addictiveness
			harm to users


				alcohol:
					major problem
					more addictive than heroin
					more dangerous
					socially accepted

				nicotine:
					allen carr


### homelessness
	how many people experience homelessness?
	homelessness alcohol story
	support networks

### consumption
	online media
	food
	thought speed



## Needs
calmer society
	addictions
	consumption
	homelessness
uptake of a calm computing replacement

	urbit
		tlon survey
		calm
		distributed computing




%
%%
# Assertions
%%
%

The assertions section is your chance to describe how you’re going to change things.

We will do X,
and then Y will happen.
We will build Z with this much money in this much time.
We will present Q to the market,
and the market will respond by taking this action.

	You’re creating tension by telling stories.
	You’re serving a specific market.
	You’re expecting something to happen because of your arrival. What?

This is the heart of the modern business plan.

The only reason to launch a project is to make change,
to make things better,
and we want to know what you’re going to do and what impact it’s going to have.

---
## vision
	no nut november
		awareness
			memes
		celebrities
			terry cews
			billie elish
		influencers
			trends
		media
			conversation already havig it
		local action
			posters
			outreach campaigns
	based
		freedom
		possible
		common scams
			myopia
		desire to help others
		themes
			anti-consumption
			practicality
				growing food at home
				learning skills
		alcohol
			open source

	based toolkit
		posters
		social media content
		letters
			politicans
			own schools
			universities
		influencers


	urbit coordination
		communitites
		tools
			listen
				chat with others, calmly
			find
				people doing things
			learn
				skills from others
			create
				things under tags??
			share (sell)
				other people's stuff

		barrier to entry
			spaceship
			bluetooth / wifi

		effects of urbit
			calming
		        	society

		compounding
			improving local communities
			growing food
				potatoes
				donating
			homelessness
			"what's your urbit"

### NNN
	will happen as usual
	celebrities
	springboard:
		publicity:
			celebrities
			news
			tech youtuber

	meme magic:
		coomer
			(banned)
			go to urbit

	who:
			young people
			organisations
				fight the new drug
				your brain on porn
				young people organisations

			islamic countries
			religious leaders
			clandestine operations


### based messaging
	// why not consumption?
		wall-e

		problems with tv
		supermarkets
		examples of ccp
			programming
			time to end ccp
			love
			peace is an option
			drop the wall
				what do they see?
			not based on truth
				cannot stand
			left right

### based change (toolkit)
	sharing based method
	urbit
	wacko schemes
		teams
	thinking
        posters
        local groups
        churches
        conversation
        schools
	sports groups



	For instance, they might print posters with a 'NNN Cheat Code', or share

	// what are the shared goals?
		ending porn

	// how will people calm their communitites.
		by sharing qutting pmo messaging
		organising local events
		attending local
		addiction resources
		feeling good

	// how to leave people loving each other
		waking up
		smallness
		critical thinking


### urbit
	listen
		chat with others, calmly
	find
		people doing things
	learn
		skills from others
	create
		things under tags??
	share (sell)
		other people's stuff


	urbit culture
		accepting
		cultural osmosis
		above
		because the alternative
			community fracture



### Addictions
	kick the drink easily
	easyway
		open source
		plain packaging


### Cultural shifts
	practical
	compassionate
	friendly



#### life
	switching friends to urbit
		chat program
		bluetooth / wifi
	critical thinking
	sam harris
	gardening at home
		donating
			homelessness shelters
				alcohol addiction
				volunteer
		marketing benefits
		helping out
		neighbours
		complexes

	hobbies:
		growing food
		yoga
		[other hobbies]

	chatting with others

### Cultural shifts
	dharma
	sharing
	kindness
	calming



%
%%
# Alternatives
%%
%

You will make assertions (above) that won’t pan out.

You’ll miss budgets and deadlines and sales.
So, the alternatives section tells me what you’ll do if that happens.
How much flexibility does your product or team have?
If your assertions don’t pan out, is it over?

hundreds of millions
	not hundreds of millions
		media attention
		every NNN
		snowball

	people don't community funnel
		some will
		support
		failsafe
			media
			religion

book isn't good
	trying my best
	failsafe: old book

not agreeing with message
	"sorry, it's not for you."
	global warming inevitable

urbit tools not done
	still can chat
	projects right now

people don't use urbit
	early adopting
		technical barrier
		slope
	only a matter of time

	reducing barriers

news
	conversations already starting
	celebrities

porn industry
	threat
	truth
		studies
			nicole prause
			[other biased researchers]
	news
		speaking out
	astroturfing
	love

	models speaking out
		counteract, "come to urbit!"
		abuse
		trauma
		interviewing models

deadlines
	urbit apps
	book
	translations
	toolkit

%
%%
# People
%%
%

The people section rightly highlights the key element:
	Who is on your team

	who is going to join your team.
	“Who” doesn’t mean their resumes;
		attitudes, abilities
		track record in shipping.

You can go further here.
	Who are people you’re serving?
	Who are the champions?
	What do they believe about status?
	What worldview do they have?

### team formation
imagine or conceptualise
team building activity
	in person
		nature
		we are not really strangers
		no alcohol
	remotely
		we are not really strangers



alliance
basecamp

XXX TODO SPEAK TO ILKAI

[each organisation]
	similar messaging

personal team
	fraser
		writing
		vision
	logistics (Cindy Pham)
		organising team
		reducing thought load

	publicist
		media teams

	grassroots (Athina Hilman)
		finding organisations

	creativity (XXX)
		honestly, kanye west would be ideal
		look at jre interview, he'd have a field day

	Julian
		Julian Harris

	practical
		Josh Holbrook

	Urbit
		Josh Lehman

	flights
	food
	accomodation

health team
	jason vale -- alcohol and juice
	easyway head -- nicotine and sugar
	jordan peterson -- philosophy and organising
	sam harris -- neuroscience
	dr john campbell -- communicating authentically
	psychologist
	vibes

coordination team
		best project managers
		assistants

urbit
	foundation
	tlon rep
	engineer
	engineer 2
	engineer 3

media team
	publicist
		assistant
	publicist
	seth godin (ideally)



porn
	fight th new drug
	your brain on porn
	every other porn organisation


specialists
	accountant (cindy temp)
	lawyer


homelessness
	food bank
	aclu
	homelessness org 1
	homelessness org 2
	homelessness org 3
        homelessness org 4
	homelessness org 5
	homelessness org 6
	homelessness org 7

religious
	christian
	anglican
	catholic
	lutherian
	muslim
	buddist
	hindi
	sikh
	peacemaker

	+ 9 more in other team


environmental
	greenpeace
	greta
	wwf
	charity
	charity
	political org
	logistics
	environmental engineering

running based
	fraser
	exec
	customer service
	customer service 2




%
%%
## Money
%%
%

The last section is all about money.
How much you need

how you will spend it
	initially, giving planets to contributors for friends and family


what cash flow looks like
	(currently not selling due to fair use)
	working with positiveidentity
	book sales
	large waterbottles with good handle (wood?)
	shirts
	flap hats
	self serving promotion


--
need not require money
	moves faster if


travelling around country
media appearances

urbit planets
	giving some to contributors
	urbit planet blood donation

stars
	hybrid teams
	build teams of people to onboard a demographic

online sponsorship
	luke smith
	other counterculture computing
	counterculture twitter

artistry
	hypercommunications
		startup.mp4
		people from art film
		jon rafman??
	team for media
		outreach

tlon (need to know more)
	computing pool
		sell unused computing space from urbits?
	dao for managing people land food production
	engineering for companies


The project need not require money -- it's a grassroots campaign -- but funding moves it infinitely quicker.


## Future

anti-ccp
	love
	what will people see
ending golf
voting systems

tlon
	global computing
		bits (urbs? -- for people?)

end golf
	purposeful protest
	homelessness
	fasting
	peacefulness

## importance

war
suffering
suicides
