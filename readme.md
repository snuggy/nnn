# Proposal

Using No Nut November for giving people hope and inviting them to change their local after freeing themselves from pornography.

Goals,

0. Curing many people worldwide of pornography and alcohol addictions
1. Directing them to Urbit to respect their attention
2. Giving them tools for improving their communities.

*This readme acts as a shorter (and better) proposal; it'll be a short slide deck soon.*

## Structure

[Truth](truth.md) -- Agreeing what the world is like.

[Assertions](assertions.md) -- How we can change it.

[Alternatives](alternatives.md) -- Encountering challenges.

[People](people.md) -- Who we've need.

[Money](money.md) -- Money and stars.

## Overview

I run [easypeasymethod.org](https://easypeasymethod.org) which is regarded as the de-facto method of quitting pornography. It gets [60 to 80 thousand](https://plausible.io/easypeasymethod.org) monthly vistors, growing purely organically since 2020. It's a rewritten rewrite of Allen Carr's *EasyWay to Stop Smoking* and is completely free and open source, offering an approach without willpower instead of 'streak counting' or 'recovering' for life. Chances are that you'll be recommended it online.

I'm [rewriting it](https://gitlab.com/snuggy/based) to be clearer and consise. It's aiming to be more of an experience by using people's memories, imaginations and guiding them to their own realisations. It'll be done by 'No Nut November' -- a global abstience trend -- providing an opportunity for global reach through [machine learning](https://deepl.com) translations with [crowdsourced](https://weblate.org) fixes, massively cutting down translation time.

After quitting porn, I want people to stop caring about it; focusing energy to calming their local communities instead.

Urbit is the only tool for the job.

I propose creating a couple of simple tools for connecting people globally, finding local events, learning skills, sharing creations and managing projects. I'll describe this shortly, I'd first like to show porn's ubiquity and pervasiveness.

## Pornography

Internet pornography is [extremely damaging](https://www.yourbrainonporn.com/miscellaneous-resources/start-here-evolution-has-not-prepared-your-brain-for-todays-porn/) because we can't handle endless novelty. When there's more dopamine than would normally be possible you build rapid tolerance to it. Therefore, you begin requring more and more pornography or different escalating genres. With frequent dopamine flooding, our primal reward system begins trimming other receptors, and depression, anxiety, stress and many other conditions are often *primary* symptoms of porn use.

I ask you remove the notion of a 'porn addict' and see this as a spectrum.

> "*Many ask the obvious question: “How much is too much?” This question presumes that porn’s effects are binary. That is, you either have no problem, or you are a porn addict. However, porn-induced brain changes occur on a spectrum and cannot be classified as black and white, either/or. Asking where one crosses the line ignores the principle of neuroplasticity: the brain is always learning, changing and adapting in response to the environment.*"
>
> --- Gary Wilson, '[Evolution has not prepared your brain for internet porn](https://www.yourbrainonporn.com/miscellaneous-resources/start-here-evolution-has-not-prepared-your-brain-for-todays-porn/)'

It's estimated that 40 million North American adults regularly use pornography but frankly this intuitively feels incorrect. ~~A Czech study asking "do you use pornography" to XXX people had 95.6% XXX of men and 65.7% XXX of women using (lost this citation oops)~~. Nearly every adult I've spoken to laments over their partner's pornography use or uses it themselves. It's not just western countries either -- I often speak to people in heavily religious (Islamic) countries who were cured through using EasyPeasy, detailing almost every other young person at their school using it.

We're seeing the effects today. I speak to school nurses who have teenage girls crying in their offices because their *eleven* year old boyfriends are pressuring them for anal sex.

Again: there's no such thing as a porn addict, just varying lengths of addiction to a supernormal stimulus that our reward system cannot handle.

People finding themselves addicted usually self-stigmatise because porn is ubiqutious and place the blame on themselves. Quitting using willpower -- anything involving counting, blocking, dieting, rewarding or punishing -- leaves people in depressive relapse cycles because porn becomes a 'forbidden fruit' and willpower is finite.

Zero willpower approaches work by targetting the reasons why people use pornography.

[Allen Carr's EasyWay](https://easyway.com) has helped tens of millions, with clinics boasting 95% success rates with money back guarantees. I'm also a big fan of Jason Vale's [Kick the Drink... Easily!] which is highly effective for quitting alcohol. I believe making these methods open source would increase their reach, impact, and profits (through brand awareness book sales) for their authors.

Quitting
	statistics
		nnn
		nofap
		pornfree
		fight the new drug


---

EasyPeasy's complete rewrite works more effectively by guiding people into having their own realisations and leaving them in a 'higher state of consciousness' (borrowing from Think and Grow Rich).

From this, consumption itself is questioned: both online (social and mass media) and offline (supermarkets, surrogate activites, sugar).

The goal is leaving them with hope, personal responsibility, and easily actionable steps for influencing their local communities.

Someone newly freed from pornography will have a toolkit with resources -- like posters, social media posts, school board petitions, political activism guides -- and ways of finding others in similar mindsets.

However, I don't want people obsessing over porngraphy. I'd like them to *live life* free of it.

I imagine a world where people are growing their own produce (*anyone* can grow potatoes) and donating food to people in homelessness, onboarding their neighbours, and creating communities with less organisational dependence.

---

[Urbit](https://urbit.org) -- a drop in replacement for the internet and computing itself -- is the only tool for the job.

Why?
	- The network is run by people, not corporations
	- Allows easily creating communities
	- Calm and respects attention (*why give people another addiction?*)
	- Completely separate from profit driven companies
	- Flexible, as anything can be built on this general purpose computer.

I propose creating a small suite of tools for people to influence their local communities.

## Urbit tool ideas

Developing tools would be agile. Javascript frameworks such as [Svel](https://svelte.dev) [(explained)](https://youtu.be/rv3Yq-B8qp4) can simplify and speed up development.

Here's five tools to get started.

**Listen**

	Have anonymous conversations with others around the world.

	Instilling awareness and compassion.

**Find**

	Events, volunteering opportunities and jobs.

	Connecting with local communities.

**Learn**

	Guides written by people. [Minimal](https://based.cooking) and general.

	Giving people skills to help the world.

**Share**

	Creations. Accept donations easily. Other planets can act as escrow.

	Fun ways of making money.

**Do**
	Nested to-do lists with dates for teams.

	For working together easily.


To lower barriers to entry, an open source chat app -- such as Matrix or Signal -- can be forked to use use Urbit ID. It doesn't have to be perfect, it's just having something. It's killer feature should be a really, really good notifications system.

At minimum, it should allow controlling:
	- Read receipts
	- Fine-grained notifications on a per chat basis (e.g. an end-of-day message digest)
	- Sending automated responses asking if the message should be read immediately.

In an ideal world, this app would work without an internet connection and connect to others wifi and bluetooth connections for easier uptake in developing nations.

Developers and contributors should be given stars for completing these.

### Why...

***is this good for urbit?***

Succeeds in bringing calm people to the platform. Urbit's 'Eternal September' is inevitable and uncontrollable, but can be guided with good values.

***is quitting porn good for the world?***

Quitting addictions naturally calms people; by cultivating hope, and giving tools for calming their local communities, large scale cultural change is easy.

***is using urbit is good for the world?***

Social platforms are causing division because we're trapped in addictive little bubbles. The only way out is an [entirely new platform that's owned and controlled by it's users.](https://urbit.org/overview)

Where will this path lead?

### World Wide Rave

I like Seth Godin, who pioneered 'permission-based marketing', and his book 'World Wide Rave' which includes the story of how Cindy Gordon, vice president of New Media Marketing at Universal Orlando Resort, launched The Wizarding World of Harry Potter (a very impressive and immersive Hogsmeade Harry Potter theme park experience.)

She told seven hand people selected from a famous Harry Potter forum, who told seven people, and so on, so forth.

Gordon estimates that 350 million people globally heard the news that Universal Orlando Resort was creating The Wizarding World of Harry Potter theme park -- all by telling just seven people.


## People

Taking a more general approach than seven people will be more effective, because EasyPeasy already has an existing reputation.

Target audiences can be grouped:

**Addicted, struggling** -- People struggling using willpower approaches who are receptive, but unaware of no willpower approaches.

	Targetting: Use existing addiction communities for promoting the method and give people tools for doing the same.

**No Nut November** -- People who have hopped on, or are aware, of the No Nut November trend.

	Targetting: Use memes with customisable watermarks spread by real people to share the message.

**Quitting organisations** -- Such as [Fight the New Drug](https://fightthenewdrug.org) and [Your Brain on Porn](https://yourbrainonporn.com), subreddits and many others.

	Targetting: Connect organisations together for promoting the harmful effects of pornography, which further publicises the anti-porn movement.

**Media** -- National media networks, such as television and radio.

	Targetting: Porn is already being discussed in the media. Goal is bridging the political divide by de-politicising the issue by pairing with mothers groups.

**Technology** -- Neophilles who are looking for the 'next big thing'.

	Targetting: Technology influencers interviewing with Urbit representatives.

**Religious** -- Religious organisations who's members use pornography, but aren't comfortable talking about it.

	Targetting: Arguments against lust, alongside collaboration with other relgions for unity. Must be framed without judgement for users, as increasing stigma will not help.

	Faith based influencers would help too.

**Celebrities** -- Who have spoken out against porn.

	Targetting: Interview celebrities as people, not 'former porn addicts' and showing the positives of lives free from pornography. Simply, it's better PR.

**Self-improvement** -- People who are working on themselves and being productive.

	Targetting: Using toolkit for getting fans of influencers to publicise themselves. For individual people, Urbit is marketed as a tool to reduce distraction.

**Partners** -- Of porn users and other substances (such as alcohol)

	Targetting: Contacting admins of online partner communities and making them aware of new approaches.

**Addiction recovery** -- Organisations treating addictions, or vulnerable people affected by them (such as homelessness organisations)

	Targetting -- Send links to open source addiction recovery books to homelessness shelters and addiction treatment centres.

**Shitposting** -- People who aren't interested in taking local action, but like mems.

	Strategy: Bring back the [Coomer meme](https://coomer.org) with a way of sharing Urbit at the same time.

### Meme Magic

Controlling the internet isn't possible, but we can show people better values.

Show hatred and invite them to stop, promoting compassion and understanding instead.

Coomer became a big meme because it's genuinely funny and showed a very real social problem. Problems arose when hate was allowed to spread within the community, much to the genuine distaste of the majority.

It'll become even bigger because it's banned, and you add customisable watermarks for driving people to Urbit where it isn't.

#### Watermarking

Someone should be able to upload an image or video and add a footer containing emojis and an Urbit ID to customise it.

#### Hypercommunications

'Hypercommunications' on anti-consumption and visions of the future. Similar to Jon Rafman or startup.mp4 but with coherient messaging.


## Local actions

- People should be able to generate posters in the same way as the 'watermarker' but promoting a 'NNN Cheat Code' alongside an Urbit ID
- Instructions on reaching out to local interested communities, like churches, sport clubs and youth centres
- Talking to their school for incorporating pornography education into sexual education programs.

### Team

*still working on this section*, doing it on the plane tomorrow

**Fraser**

	Writing
	Public face (semi-anonymous by wearing [this hat](https://www.solbari.com/collections/men-sun-hats/products/men-wide-brim-with-face-shield-sun-hat-upf50?variant=40773260181615) with sunglasses)

**Logistics / Accounting** -- Cindy Pham

	organising team
	reducing thought load

**Publicist** -- [still looking]

**Grassroots** -- Athina Hilman

	sculpting grassroots change

**Business** -- [undecided]

**Writing** -- Josh Holbrook

**Frontend** -- Vicete Amus

**Urbit** -- Josh Lehman

*more in* [_path.md](_path.md) -- *in short, must bring diverse people together*


## Money

I should mention that regardless of Urbit, I plan on spreading EasyPeasy.

I've never monitised EasyPeasy because it's an edit of Allen Carr. With the new rewrite I'll begin selling physical books and useful merchandise, such as subtlely branded waterbottles and hats.

I'll use that money for travelling around the country -- first to Utah and Idaho for their high porn use, yet unsupportive populations --  moving from conservative to progressively more liberal states.

For now, I just want an 'ok' from the Urbit Foundation. Many people coming to Urbit means infrastructure upgrades for ensuring a good user experience.

Two other things would be useful.

- Urbit IDs
- Developers.

### Urbit IDs

People who either

- Are active in the community
- Do something cool for promoting Urbit or EasyPeasy
- Translate the book
- Have large audiences
- Run in-person communities

Should be given urbit planets for their friends, families or communities.

As the movement spreads, people will also be required to make a blood donation.

### Developers

People developing the Urbit tools above, alongside other imagined tools for community engagement should be paid in stars for development (similar to the current grant program).

### Other expenses

# Why?

What path are we travelling down?
