## Assertions

Quitting addictions naturally calms people; by cultivating hope, and giving tools for calming their local communities, large scale cultural change is easy.

*What to do with millions of people who are grateful for their freedom from pornography?*

I'm using this 'higher state of consciousness' for questioning consumption (social media, supermarkets, mass media, surrogate activities) and driving people into calming their local communities.

We can use calm tools on Urbit are used for authetically chatting with others and organising local action,

This isn't limited to pornography, but infinite numbers of local problems.


## Urbit tool ideas

Developing tools would be agile. Javascript frameworks such as [Svelte](https://svelte.dev) (XXX EXPLAINER) can speed up development and polish can come later as development communities grow.

Here's five tools to get started.


**Listen**
	Have anonymous conversations with others around the world.
	Instilling awareness and compassion.

**Find**
	Events, volunteering opportunities and jobs.
	Connecting with local communities.

**Learn**
	Guides written by people. [Minimal](https://based.cooking) and general.
	Giving people skills to help the world.

**Share**
	Creations. Accept donations easily. Other planets can act as escrow.
	Fun ways of making money.

**Do**
	Nested to-do lists with dates for teams.
	For working together easily.



Urbit might also take an open source chat app -- such as Matrix or Signal -- and have it use Urbit ID. It doesn't have to be perfect, it's just having something. It's killer feature should be a really, really good notifications system.

At minimum, it should allow controlling:
	- Read receipts
	- Fine-grained notifications on a per chat basis (e.g. an end-of-day message digest)
	- Sending automated responses asking if the message should be read immediately.

In an ideal world, this app would work without an internet connection and connect to others wifi and bluetooth connections for easier uptake in developing nations.

We should start with XXX, who are currently attempting to develop their own internet.

Additionally, other open source games and applications might also be modifed to use Urbit ID, or their developers be given money and Urbit address space to do it.

## Vision

Urbit is ubiquitious and creating mass social change. Porn and alcohol stopping. Many people fasting and growing food. Social media is seen like vaping, something heaps of people are doing but many people don't. Urbit's just calmer, you know?

After quitting porn, you'd enjoy your life and spread the vibe. You'd actually see posters around university campuses. "Urbit's actually good" -- they'd say -- "are you running a planet yet?". Life's calmer, and time is saved. You're working with others for helping the world. You're taking personal action in spreading this feeling.

Social platforms are becoming barren -- mentioning addictions and the attention economy -- social media censoring only fanning the flames.

Helping the world is your vibe. Spending time online isn't.

Plus, you're one of many doing the same.

Donating food feels good, homelessness donations doublely good, and are paired with free addiction toolkits. People taking control of their lives snowballing through the community.

Politicans are interested, and teachers are working with students in campaigns combatting addiction. So too, universities -- warning students in their consent programs -- and having students consider how porn has affected them.

Overall, it's starting conversations -- about anything -- between people. It's not a movement with a name, just people becoming more present and calming the world using tools that respect them.



--

OLD VERSION:

Vividly imagine someone struggling with pornography use, finding Based Method -- either through No Nut November, influencers, media, memes or even posters -- and freed from the frustation of addiction, and filled with hope for the world.

After noticing positive changes personally, they're keen helping others to escape. Alongside social media posts, video guides and letters, the 'Based Change' toolkit has posters for promoting Urbit and quitting porn, but they're surprised finding them in their university bathrooms already.

Feeling validated, they join local Urbit communities. They don't really discuss porn -- they're non-users after all -- but local hikes, campaigns for community gardens and skills they've been learning. Most have started growing food -- potatoes at least -- and sharing it with family, neighbours and local homelessness shelters.

Homelessness donations are paired with links to free addiction toolkits. People are beginning to take control of their lives, and commonly used substances are being questioned.

Politicans are interested, and teachers are working with students in campaigns combatting addiction. So too, universities are warning students in their consent programs and causing students to question porn's normality.

Students are also questioning what they've been sold into.

Urbit is the 'hot new thing' -- whether running your own planet or using an Urbit ID chat app -- people are questioning the predatory anti-patterns of massive corporations. Coomer, Consoomer and Coomsoomer memes are commonplace, but also discussing literature from [Project Gutenberg](https://gutenberg.org) or finding hobbies.

Through Urbit's tools people begin learning more about each other and the world. *"This chick I was speaking to on Urbit had the most interesting life!".* They're learning skills that benefit their local communities and finding ways to be part of them. People share their art in customisable portfolios, and selling it without companies taking cuts. But this is just the beginning, and Urbit's flexibility is experimented with every day.

*"What's your urbit?"*

	better vision weaved using urbit tools

OLD VERSION

---

## How?

People only ask two questions when choosing whether to support something.

1. Why is it important?
2. How can it happen?

In Based Methods' case, they're shown a vision of the future without an ideology and the steps for getting there.

If they can see it's progressing, they're more likely supporting it.

### Path

We want to use the inevitable social change for spurring along other ones.

First, writing an really good **book**, recording an audiobook for it, and then a video guide.

Pairing it with a **toolkit**, featuring
	- Designs
	- Online content
	- Posters
	- Outreaching guides
	- Letters
... and many more -- it's open source.

Online, you're popularising Coomer and **memes** with customisable emoji watermarks for starting conversations.

These **conversations** aren't just limited to users, but everyone. Mothers, pastors, girlfriends, wives, and people begin questioning their assumptions on many addictions.

One such assumption is using online services run by advertising companies. People are switching to **Urbit**, either through running their own planet or using their Urbit ID with a chat application.

They begin listening to others, connecting with their their local communities, learning skills to influence them, begin sharing their creations, and working effectively with others. Everyone is taking note.

	// how are people connecting

It's cool growing your own **food**; a fact spread by influencers, celebrities, media reports, religious leaders and schools. Donating it is even cooler.

Indeed, heaps of people have quit other **addictions** like alcohol and nicotine, and have begun taking back control of their lives.

These positive feedback loops keep gaining more attention with leaders and policy makers, starting to add more supportive policies.

*When has top down revolution ever worked? Change starts with you.*

### Communities

There's infinite communities who'll be interested, but broadly.

**Addictions** -- For those quitting porn, alcohol and other substances interested in hearing new approaches that work.

**Partners of users** -- Read [r/loveafterporn](https://old.reddit.com/r/loveafterporn) and consider the millions knowingly and unknowingly in similar situations. So too, for alcohol (domestic violence, support services, families) and other substances.

*Sidenote: Create trends of partners sharing porn's impacts and having single girls reject guys who watch porn. You could even create a trend of tantra until next year.*

**Support services** -- Such as those for alcohol, sex or homelessness, who can benefit from free addiction toolkits.

**Schools** -- Educating against porn addiction on low budgets.

**Technical** -- People interested in technology that likely use themselves.

### Celebrities

Many celebrities have already spoken out against porn. Instead of interviewing them about it, we'd just be vibe instead -- touching on the subject, but just by saying how good life is without it.

The goal is combatting stigmatisation of pornography addiction, not raising awareness of it. Your showing is showing celebrities as people, not their addictions.

### Media

With media attention, the goal is starting non-judgemental mentionings about porn in families and highlighting the positive changes coming about from people quitting.

## Religion

I'll sound clinical here, but religions are effective ways of sharing beliefs. The problem isn't limited to the west; I speak with many young people in heavily religious countries, where nearly every young person they know uses porn.

If there's one thing the major religions agree on, it's that lust is a lower state of consciousness. For change, conversations must be approached without persecution.

For this, I propose getting different relgious together with the common goal of spreading the message as far as possible.

### Meme magic

Controlling the internet isn't possible, but we can show people better values.

Show hatred and invite them to stop, promoting compassion and understanding instead.

Coomer became a big meme because it's genuinely funny and showed a very real social problem. Problems arose when hate was allowed to spread within the community, much to the genuine distaste of the majority.

It'll become even bigger because it's banned, and you add customisable watermarks for driving people to Urbit where it isn't.

#### Watermarking

Someone should be able to upload an image or video and add a footer containing emojis and an Urbit ID to customise it.

#### Hypercommunications

'Hypercommunications' on anti-consumption and visions of the future. Similar to Jon Rafman, or startup.mp4 but with coherient messaging.

## Culture

Volunteering
openness
kind
compassion
sharing
hobbies
	yoga
	pilates
	languagetransfer.org
	outside
	sharing knowledge

Overall, the goal is nurturing a compassionate culture. We tend to focus on the extremes, but most people I meet are lovely. Regardless, how would attacking anyone help?
