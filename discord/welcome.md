# Welcome

It's lovely having you here.
We're using the momentum from No Nut November for bringing about world peace.

I know this sounds like an intense goal, but it really isn't.
We just need to cure everyone of their addictions, give them an urbit planet, and coordinate local communities.

Everyone's stressing about the price of food and fuel, so the solution is *growing* food and sharing it between people so that communities can become self suffient.

That's the goal at a high level, so I ask that you please ***stop being stressed about the world*** and start being hopeful.

It's honestly a lot more fun.

---

We're in stage zero:

Stage 0: 
    Spreading the current version of easypeasy as far as possible for a snowball effect.

Stage 1:
    Addiction toolkit for alcohol, nicotine and pornograhy with media and influencer attention.

Stage 2:
    Ending social media using Urbit.

Stage 3:
    Cultivating local communities using tools created on Urbit.


for stages: <https://gitlab.com/snuggy/nnn/misc/stages.md>
For urbit: <https://urbit.org/overview>



I can't fully plan everything right now because I'm rewriting easypeasy, which has to cultivate hopeful feelings within readers to induce a higher state of consciousness.

From there, we need to get Allen Carr and Jason Vale on board to cure nicotine and alcohol addictions. I'll be in Turkey, so it'll be easy for me flying to London to meet them.

Don't do it yet, but we'll be doing mass tweeting campaigns at celebrities too. I'll be prepared flying anywhere to meet them, and we can let them know that.

I expect we'll have fully moved to stage one half way through November, where the real fun begins.

----

For now I need to know who you are, have you being calm and ready, and I need you to experiment.

The campaign will thrive with:

1. People moving offline.
2. Not *hating* other people, which is unfortunate within the quitting porn community and only alienates people who are suffering just as much as you.
3. People taking their own action.

Remember: We're all in this together; there is only we!



Again, our goals are:
1. easypeasy worldwide, alongside an adddiction toolkit
2. urbit, with calmness
3. a sense of community, coordinated with urbit.



## Calming yourself outside

First, spend some time in nature enjoying your freedom.

I've put this first because it's important you're
    - Changing your routine
    - Spending some chilled time alone
    - Doing something 'real'.

But while you're going around, put up posters in as many bathrooms as you can.
I'd prefer if you weren't listening to music while you doing this, instead thinking of ways you can spread easypeasy and improve your local communities.

### Posters

You'll need

- A printer, or some money for a copy shop
- [easypeasy poster designs] (or make your own!)
- Some sticky tape.

Some ideas,
    - Parks
    - Universities
    - Notice boards
    - Bars
    - Fast food resturaunts

After doing it, you can also tweet your accomplishments by using #easypeasyposters #easypeasymethod and #nonutnovember

Which, is an elegant transition to what you can do...

## Online

By posting about easypeasy through
    - Using the [easypeasy watermarker] and sharing to socials
        <https://> (swatxcats is just finalising)
    - Sharing your successes using easypeasy
    - Recommending easypeasy to others struggling with willpower.


I'd also appreciate you following:
    - coomer.memes on twitter and instagram
        <https://twitter.com/coomermemes>
        <https://instagram.com/coomer.memes>
    - fraser's social media
        (becuae I need to prove I've got an audience to get on the news -- I *do not* want to be an influencer)
        <https://instagram.com/deleteinstaplease>
        <https://twitter.com/deletebirdpls>

Soon, you'll be asked to mass tweet celebrities and organisations, coordianted through the 'everyone' forum on discord.

Alongside this, start...

## Creating

stuff, using your skills!

While you're out and about, think of creative ideas for spreading easypeasy through:

- art
- music
- videos
- memes
- programming

I'll have more direction for this soon, but for now just experiment and see what works.
I know this is very vague, but it's sort of fun -- isn't it?


Chat soon,
Fraser
